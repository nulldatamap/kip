{-# LANGUAGE OverloadedStrings #-}
module Kip (parseAndRun
           , Metavar(..)
           , Program(..)
           , Fragment(..)
           , VerbItem(..)
           , Verbatim(..)
           , Content(..)
           , Def(..)
           , Form(..)
           , Exp(..)
           , pExp
           , pVar
           , pFragment
           , pVerb
           , pContent
           , pDef
           , pForm
           , pProgram
           , pSyntaxRule
           , pString
           , pInt
           , pIdent
           , pRuleName
           , pSymbol)
  where

import Data.Functor (($>))
import Control.Monad.RWS
import Data.Map (Map)
import Data.Text (Text)
import Data.Maybe (isJust)
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Text.Parsec
import Text.Parsec.Text
import Text.Parsec.Char

data Metavar = Metavar Text Text deriving (Show, Eq)
type Ident = Text
type Symbol = Text
type Keyword = Text

data Program = Program { config :: [(Text, Text)]
                       , metavars :: Map Text Text
                       , syntaxes :: [(TokenTree, Content)]
                       , fragments :: [Fragment] }
  deriving Show

data Fragment = Fragment { fragInline :: Bool
                         , fragMath :: Bool
                         , filename :: Text
                         , contents :: [Content] }
  deriving Show

data VerbItem = VText Text | VInsert Content
  deriving Show

data Verbatim = Verb [VerbItem]
  deriving Show

data Content = CVerb Verbatim
  | CDef Def
  | CForm Form
  | CExp Exp
  deriving Show

data Def = Def Metavar Form
  deriving Show

data Size = Big | Small
  deriving (Show, Eq)

data Form = FDeriv [Prem] (Maybe (Text, Size)) (Maybe Content) Form
          | FNamed Metavar Form
          | FJud TokenTree
  deriving Show

data Prem = PForm Form | PNl
  deriving Show

data Exp = EVar Metavar
         | ESym Symbol
         | EQuote Exp
         | SExp Ident [Exp]
         | ENum Int
  deriving (Show, Eq)

data TokenTree = TList [TokenTree]
  | TSymbol Text
  | TExp Exp
  deriving Show

keywords :: [Text]
keywords = [ "config", "end", "def", "syntax", "metavar"
           , "fragment", "math", "inline", "break"]

pTok :: Parser a -> Parser a
pTok p = p <* spaces

pSym s = pTok (string s)

pMetaChar :: Parser Char
pMetaChar = digit <|> char '\'' <|> letter

pVar :: Parser Metavar
pVar = pTok (Metavar <$> pVarIdent <*> (T.pack <$> many pMetaChar)) <?> "Meta variable"

pVarIdent :: Parser Ident
pVarIdent = try $ do id <- (T.pack <$> ((:) <$> letter <*> many (char '-' <|> letter)))
                     if id `elem` keywords
                       then parserFail "Expected indentifier, got keyword"
                       else return id
                      <?> "Identifier"

pIdent = pTok $ T.pack <$> many1 (alphaNum <|> oneOf "!@#$^&*_?+-/|*<>:;.,")

pRuleName :: Parser (Text, Size)
pRuleName = (flip (,)) <$> (option Big (char '_' $> Small))
                       <*> (pTok $ T.pack <$> (many (letter <|> digit <|> oneOf "-_!?*'")))

pKeyword :: Parser Keyword
pKeyword = pTok $ char '\\' *> (T.pack <$> many1 (alphaNum <|> oneOf ".-_?!*"))

pSymbol :: Parser Symbol
pSymbol = try (do s <- pTok $ T.cons <$> symChar
                                     <*> (T.pack <$> many (symChar <|> digit <|> oneOf "'_-[]"))
                  if s `elem` ["---", "="]
                    then parserFail $ "`" ++ (T.unpack s) ++ "` is a reserved symbol"
                    else return s)
  where
    symChar = oneOf "<>{}=-!@#$*^&+-/|,.:"

pInt :: Parser Int
pInt = pTok (read <$> (many1 digit)) <?> "Integer literal"

pString :: Parser Text
pString = pTok (between (char '"') (char '"') $ T.pack <$> many escapeOrAny)
  where
    escapeOrAny = (char '!' *> (char '!' <|> char '"')) <|> noneOf "\""

pDef :: Parser Def
pDef = pSym "def" *> (Def <$> pVar <*> (pSym "=" *> pForm)) <?> "Definition"

pParen :: Parser a -> Parser a
pParen = between (pSym "(") (pSym ")")

pABrac :: Parser a -> Parser a
pABrac = between (pSym "<") (pSym ">")

pBrac :: Parser a -> Parser a
pBrac = between (pSym "[") (pSym "]")

pBrace :: Parser a -> Parser a
pBrace = between (pSym "{") (pSym "}")

pForm :: Parser Form
pForm = try (FNamed <$> pVar <*> (pSym ":" *> pForm))
  <|> FJud <$> (pSym "," *> pTts1)
  <|> try (FDeriv <$> many pPrem
                  <*> optionMaybe (pBrac pRuleName)
                  <*> (pSym "---" *> optionMaybe (pBrac pNote))
                  <*> pForm)
  <|> FJud <$> pTts1
  <?> "derivation form"
  where
    pNote = CVerb <$> pVerb
         <|> CExp <$> pExp
    pPrem = PForm <$> (pParen pForm)
         <|> pSym "#" $> PNl

pExp :: Parser Exp
pExp = char '\'' *> (EQuote <$> pExp)
  <|> pParen (SExp <$> pIdent <*> many pExp)
  <|> ESym <$> pKeyword
  <|> EVar <$> pVar
  <|> ENum <$> pInt
  <?> "expression"

pSyntaxRule :: Parser (TokenTree, Content)
pSyntaxRule = pSym "syntax" *> ((,) <$> pTts1 <*> (pSym "=" *> pContent))

pProgram :: Parser Program
pProgram = spaces *>
           (Program <$> pOptions
            <*> (M.fromList <$> many pMetavarDef)
            <*> many pSyntaxRule
            <*> (concat <$> many pFragment)
            <* eof)
  where
    pOptions = between (pSym "config") (pSym "end")
               (many ((,) <$> (pTok pVarIdent) <*> (pSym "=" *> pString)))
    pMetavarDef = pSym "metavar" *>
                    (makeMetavar <$> (pTok pVarIdent)
                                 <*> (optionMaybe $ pSym "=" *> pString))
    makeMetavar mv Nothing = (mv, mv)
    makeMetavar mv (Just x) = (mv, x)

pFragment :: Parser [Fragment]
pFragment = do
  brk <- isJust <$> (optionMaybe $ pSym "break")
  inl <- isJust <$> (optionMaybe $ pSym "inline")
  mat <- isJust <$> (optionMaybe $ pSym "math")
  between (pSym "fragment") (pSym "end")
          ((fragment brk inl mat) <$> pString <*> (many pContent))
  where
    fragment True inl mat name content =
      if isJust $ T.find ('%'==) name
      then map (\(c, i) ->
                  let name' = T.replace "%" (T.pack $ show i) name
                  in Fragment inl mat name' [c]) $ zip content [0..]
      else fail $ "Can't break fragment, doesn't have a name-template: " ++ T.unpack name
    fragment False inl mat name content =
      [Fragment inl mat name content]

pInlineContent :: Parser Content
pInlineContent =
  CDef <$> pDef
  <|> CForm <$> pForm
  <|> CExp <$> pExp

pContent :: Parser Content
pContent =
  CVerb <$> pVerb
  <|> pInlineContent <* pSym ";"

pVerb :: Parser Verbatim
pVerb = pTok (Verb <$> ((pSym "%{") *> many item <* (pSym "%}")))
  where
    item = (try $ char '%' <* notFollowedBy (char '}')) *>
           (VInsert <$> ((pSym "[" *> pInlineContent <* char ']')
                         <|> CExp <$> pExp))
      <|> (VText . T.pack) <$> (many1 (noneOf "%"))

ttList [t] = t
ttList ts = TList ts

pTts = ttList <$> many pTt
pTts1 = ttList <$> many1 pTt

pTt :: Parser TokenTree
pTt = TSymbol <$> pSymbol
   <|> TExp <$> pExp

data Context =
  Context { metavarRules :: Map Text Text
          , syntaxRules :: [(TokenTree, Content)]
          , premiseSpacing :: Text
          , smallFont :: Text }

class Subst a where
  subst :: Metavar -> Exp -> a -> a

instance Subst Exp where
  subst mv r mve@(EVar mv') = if mv == mv' then r else mve
  subst mv r (SExp h xs) = SExp h $ map (subst mv r) xs
  subst _ _ e = e

instance Subst Form where
  subst mv r (FDeriv fs rule note f) =
    FDeriv (map (subst mv r) fs)
           rule
           ((subst mv r) <$> note) $ subst mv r f
  subst mv r (FNamed v f) = FNamed v $ subst mv r f
  subst mv r (FJud jud) = FJud $ subst mv r jud

instance Subst Prem where
  subst mv r PNl = PNl
  subst mv r (PForm f) = PForm $ subst mv r f

instance Subst Def where
  subst mv r (Def mv' f) = Def mv' $ subst mv r f

instance Subst VerbItem where
  subst mv r (VInsert c) = VInsert $ subst mv r c
  subst _ _ vi = vi

instance Subst Verbatim where
  subst mv r (Verb vis) = Verb $ map (subst mv r) vis

instance Subst Content where
  subst mv r (CVerb v) = CVerb $ subst mv r v
  subst mv r (CDef d) = CDef $ subst mv r d
  subst mv r (CForm f) = CForm $ subst mv r f
  subst mv r (CExp e) = CExp $ subst mv r e

instance Subst TokenTree where
  subst mv r (TList tts) = TList $ map (subst mv r) tts
  subst mv r (TExp e) = TExp $ subst mv r e
  subst mv r tt = tt

instance (Subst a, Subst b) => Subst (a, b) where
  subst mv e (x, y) = (subst mv e x, subst mv e y)

class Match a where
  match :: a -> a -> Maybe [(Metavar, Exp)]

instance Match a => Match [a] where
  match xs ys
    | length xs == length ys =
      foldM (\bs (x, y) ->
                fmap (bs++) $ match x y)
            [] (zip xs ys)
  match _ _ = Nothing

instance Match Exp where
    match (EVar v) e = Just [(v, e)]
    match (ESym s) (ESym s') | s == s' = Just []
    match (EQuote qe) (EQuote qe') = match qe qe'
    match (SExp h xs) (SExp h' xs') | h == h' = match xs xs'
    match (ENum i) (ENum j) | i == j = Just []
    match _ _ = Nothing

instance Match TokenTree where
  match (TList ts) (TList xs) = match ts xs
  match (TSymbol s0) (TSymbol s1) | s0 == s1 = Just []
  match (TExp (EVar v)) (TExp e) = Just $ [(v, e)]
  match (TExp (EVar _)) _ = Nothing
  match (TExp e) _ = error $ "Expressions are not allowed in judgement syntax patterns: " ++ show e
  match _ _ = Nothing

matchSyntax :: Context -> TokenTree -> Maybe Content
matchSyntax ctx tt =
  matchRules (syntaxRules ctx) tt
  where
    matchRules [] _ = Nothing
    matchRules ((r, c):rs) e =
      case mm r e of
        Just bs -> Just (foldl (\e (mv, r) -> subst mv r e) c bs)
        Nothing -> matchRules rs e
    mm (TExp pat) (TExp e) = match pat e
    mm (TExp _) tt = Nothing
    mm pat tt = match pat tt

matchExp :: Context -> Exp -> Content
matchExp ctx e =
    case matchSyntax ctx (TExp e) of
      Nothing -> case e of
                   ESym s -> CVerb $ Verb [VText "\\mathbf{", VInsert $ CExp e, VText "}"]
                   (SExp h xs) ->
                     error $ "No syntax for: " ++ T.unpack h
                                               ++ "/"
                                               ++ (show $ length xs)
      Just c -> c

patternOf :: TokenTree -> Text
patternOf tt = T.intercalate " " $ helper tt
  where
    helper :: TokenTree -> [Text]
    helper (TList ts) = concatMap helper ts
    helper (TSymbol s) = [s]
    helper (TExp e) = ["_"]

defaultContext = Context { metavarRules = M.empty
                         , premiseSpacing = "0.6cm"
                         , smallFont = "\\small"
                         , syntaxRules = [] }

type Contexter = RWS () [Text] Context

settings :: Map Text (Text -> Context -> Context)
settings = M.fromList $ [ ("premise-spacing", \x ctx -> ctx { premiseSpacing = x })
                        , ("small-font", \x ctx -> ctx { smallFont = x })]

buildContext :: Program -> (Context, [Text])
buildContext (Program cfgs mvs srs frgs) =
  execRWS builder () defaultContext
  where
    builder :: Contexter ()
    builder = do
      mapM_ tryConfig cfgs
      modify (\ctx -> ctx { metavarRules = M.union (metavarRules ctx) mvs
                          , syntaxRules = map uniqifyTemplateVars srs })
    tryConfig :: (Text, Text) -> Contexter ()
    tryConfig (set, val) =
      case M.lookup set settings of
        Nothing -> tell $ ["Warning: unknown setting: " `T.append` set]
        Just setter -> modify (setter val)

uniqifyTemplateVars :: (TokenTree, Content) -> (TokenTree, Content)
uniqifyTemplateVars (tt, c) =
  let boundVars = gatherTT tt
  in foldl (\ttc v@(Metavar vt m) -> subst v (EVar (Metavar (T.cons '%' vt) m)) ttc) (tt, c) boundVars
  where
    gatherTT (TExp (SExp _ exps)) = gatherExps exps
    gatherTT (TList ts) = gatherTTs ts
    gatherTT _ = []
    gatherTTs [] = []
    gatherTTs ((TExp (EVar v)):tts) = v : (gatherTTs tts)
    gatherTTs (_:tts) = gatherTTs tts
    gatherExps ((EVar v):es) = v : (gatherExps es)
    gatherExps (_:es) = gatherExps es
    gatherExps [] = []

parseAndRun :: String -> Text -> IO ()
parseAndRun f src =
  case parse pProgram f src of
    Left err -> putStrLn $ "Failed to parse: " ++ show err
    Right prog -> do
      let (ctx, warns) = buildContext prog
      mapM_ (putStrLn . T.unpack) warns
      mapM_ (writeLatexToFile ctx) (fragments prog)
  where
    writeLatexToFile ctx frag@(Fragment _ _ f _) = do
      TIO.writeFile (T.unpack f) $ toLatex ctx frag

data LatexState = LS { lsMath :: Bool
                     , lsInline :: Bool
                     , lsIndent :: Int }
type Latexer = RWS Context Text LatexState

line :: Latexer a -> Latexer ()
line t = do
  (LS _ inline indent) <- get
  if not inline
    then do tell $ T.replicate indent "  "
            t
            tell "\n"
    else t $> ()

line' = line . tell

scoped ::(LatexState -> LatexState) -> Latexer a -> Latexer ()
scoped mod l = do
  st <- get
  modify mod
  l
  modify (\_ -> st)

indent :: Latexer a -> Latexer ()
indent l = do
  scoped (\ls -> ls { lsIndent = (lsIndent ls) + 1 }) l

inline :: Latexer a -> Latexer ()
inline l = do
  scoped (\ls -> ls { lsInline = True }) l

isInline :: Latexer Bool
isInline = lsInline <$> get

isMath :: Latexer Bool
isMath = lsMath <$> get

text :: Latexer a -> Latexer ()
text l = do
  mat <- isMath
  if mat
    then scoped (\ls -> ls { lsMath = False, lsInline = True })
           $ do tell "\\text{"
                l
                tell "}"
    else l $> ()

math :: Latexer a -> Latexer ()
math l = do
  mat <- isMath
  if mat
    then l $> ()
    else scoped (\ls -> ls { lsMath = True })
           $ do isInl <- isInline
                line' $ if isInl then "\\(" else "\\["
                indent $ l
                line' $ if isInl then "\\)" else "\\]"

latexSep ls sep =
  foldM_ inter False ls
  where
    inter False l = l $> True
    inter True l = line' sep >> l $> True

toLatex :: Latex a => Context -> a -> Text
toLatex ctx x = snd $ evalRWS (latex x) ctx $ LS False False 0

class Latex a where
  latex :: a -> Latexer ()

instance Latex Metavar where
  latex (Metavar v m) = do
      ctx <- ask
      let (vt, es) = bestMatch v T.empty ctx
      let subscript' = es `T.append` subscript
      tell $ vt
      tell primes
      if T.null subscript'
        then return ()
        else do tell "_{"
                tell subscript'
                tell "}"
    where
      bestMatch v s ctx =
        case M.lookup v $ metavarRules ctx of
          Just m -> (m, s)
          Nothing | (T.length v) == 1 -> (v, s)
          _ -> bestMatch (T.init v) ((T.last v) `T.cons` s) ctx
      (primes, subscript) = T.span (== '\'') m

instance Latex Exp where
  latex (EVar mv) = latex mv
  latex (EQuote e) = do
    tell "\\overline{"
    latex e
    tell "}"
  latex (ENum n) = do
    tell $ T.pack $ show n
  latex e = do
    ctx <- ask
    case matchSyntax ctx (TExp e) of
      Nothing -> case e of
                   ESym s -> do tell "\\mathbf{"
                                tell s
                                tell "}"
                   (SExp h xs) ->
                     fail $ "No syntax for: " ++ T.unpack h
                                              ++ "/"
                                              ++ (show $ length xs)
      Just c -> latex c

instance Latex Form where
  latex (FDeriv ps mName mNote f) = do
    mapM_ (\(name, sz) -> do tell "\\textsc{"
                             latex sz
                             tell name
                             tell "}")
      mName
    line' "\\dfrac{"
    spacing <- premiseSpacing <$> ask
    indent $ premises ps spacing
    line' "}{"
    indent $ do line' "\\mbox{$"
                indent $ latex f
                line' "$}"
    line' "}"
    mapM_ (\note -> do tell " \\ "
                       text $ do tell "("
                                 latex note
                                 tell ")")
      mNote
      where
        premises [] spacing = line' " \\ "
        premises ps spacing = do
          let hasNls = any (\p -> case p of { PNl -> True; _ -> False }) ps
          if hasNls then line' "\\begin{array}{c}" else return ()
          latexSep (premise ps) ("\\hspace{" `T.append` spacing `T.append` "}")
          if hasNls then line' "\\end{array}" else return ()
        premise ps =
          -- If any premise has a named derivation, then we have to make
          -- phantoms for every other derivation, otherwise we'll be floating
          -- above the line.
          if any (\p -> case p of { PForm (FNamed _ _) -> True; _ -> False }) ps
          then map (\p ->
                      case p of
                        PForm (FJud j) -> do line' "\\begin{array}{c}"
                                             indent $ do line' "\\phantom{X} \\\\ "
                                             latex $ FJud j
                                             line' "\\end{array}"
                        _ -> latex p)
                   ps
            else map latex ps
  latex (FNamed mv f) = do
    line' "\\begin{array}{c} "
    indent $ do line $ do latex mv
                          tell " \\\\ "
                latex f
    line' "\\end{array}"
  latex (FJud jud) = do
    line $ latex jud

instance Latex Prem where
  latex PNl = line' " \\\\ "
  latex (PForm f) = latex f

instance Latex Size where
  latex Big = return ()
  latex Small = do
    ctx <- ask
    tell $ smallFont ctx
    tell " "

instance Latex TokenTree where
  latex tt = do
    ctx <- ask
    case matchSyntax ctx tt of
      Nothing -> fail $ "No syntax for: " ++ (T.unpack $ patternOf tt)
      Just c -> latex c

instance Latex Def where
  latex (Def mv f) = do
    latex mv
    tell " = "
    latex f

instance Latex Content where
  latex (CVerb t) = latex t
  latex (CForm f) = math $ latex f
  latex (CDef d) = math $ latex d
  latex (CExp e) = math $ latex e

instance Latex Verbatim where
  latex (Verb vs) =
    inline $ foldM_ (\wasInsert vi ->
                       case vi of
                         VText t ->
                           do if (T.head t) `notElem` (" !.,?;:()[]" :: String)
                                then tell " "
                                else return ()
                              tell t $> False
                         VInsert _ -> latex vi $> True)
               False vs

instance Latex VerbItem where
  latex (VText t) = tell t
  latex (VInsert c) = do
    latex c

instance Latex Fragment where
  latex (Fragment inl mat _ cs) =
    scoped (\ctx -> ctx { lsInline = inl
                        , lsMath = mat })
      $ mapM_ (line . latex) cs

{-- Example syntax:

E' =
  (E0: <a, s> | 1) (--- <'1, s> | 1)
  ---
  <a = 1, s> | _true
  ;

t--}

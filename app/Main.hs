module Main where

import qualified Data.Text.IO as TIO
import System.Environment
import Lib

main :: IO ()
main = do
  args <- getArgs
  mapM_ (\f -> do src <- TIO.readFile f
                  parseAndRun f src)
        args
